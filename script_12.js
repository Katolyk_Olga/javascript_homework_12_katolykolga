// Теоретичні питання
// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Не рекомендується використовувати в якості обробника події на input відслудковування події з клавіатури тому,
// що на сьогодні є багато способів заповнити input, наприклад зчитування з голоса,
// або просто вставка мишею. Краще і більш універсально для цього існує подія input.

// Завдання
// Реалізувати функцію підсвічування клавіш. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// + Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, 
// повинна фарбуватися в синій колір. 
// + При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. 
// + Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. 
// Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.


let buttons = document.querySelectorAll(".btn");
// let buttons = document.getElementsByClassName("btn");
console.log(buttons);
// console.log(buttons[0].innerText);
// console.log(buttons[4].innerText);

let letters = [];
console.log(letters);

for (let i = 0; i < buttons.length; i++) {
    letters.push(`${buttons[i].innerText}`);
}
console.log(letters);

document.addEventListener("keyup", (event) => {
    letters.forEach(function (letter, index) {
        buttons[index].style.backgroundColor = "black"});

    letters.forEach(function (letter, index) {
        if (event.code.slice(-1) === letters[index]) {            
            buttons[index].style.backgroundColor = "blue";
        } else if (event.code === "Enter") {            
            buttons[0].style.backgroundColor = "blue";
        }


    });

    // console.log(event.code);
    // console.log(event.code.slice(-1));
})



